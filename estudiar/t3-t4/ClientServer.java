

//int random = (int) (min + (Math.random() * (Max-min)));

// -- SERVER PREDET

//Si nunca se cierra
public class Server {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(8000);

            //TODO Aqui imprimir mierdas de IP

            while (true){
                Socket clientSocket = serverSocket.accept();
                DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
                
                //CODE
                
                //Cerrar cosas
                dataOutputStream.close();
                dataInputStream.close();
                clientSocket.close(); //last
            }

        serverSocket.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}

// -- CLIENT PREDET

public class Client {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            Socket socket = new Socket("127.0.0.1", 8000);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

            //CODE

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();
        } catch (IOException e){
            e.printStackTrace();
        }

        scanner.close();
    }
}
