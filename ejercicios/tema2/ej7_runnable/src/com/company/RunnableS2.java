package com.company;

public class RunnableS2 implements Runnable {

    private HiloPrincipal hiloPrincipal;
    private int z;

    // constructor
    public RunnableS2(HiloPrincipal hiloPrincipal, int z) {
        this.hiloPrincipal = hiloPrincipal;
        this.z = z;
    }

    @Override
    public void run() {
        int b = z - 1;
        hiloPrincipal.setB(b); // devolvemos la b al hilo principal
        System.out.printf("S2: b (%d) = z (%d) - 1;\n", b, z);
    }
}