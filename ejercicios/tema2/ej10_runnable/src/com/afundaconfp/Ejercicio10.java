package com.afundaconfp;

import java.util.Scanner;

//public class Ejercicio10 implements Runnable {
 public class Ejercicio10 extends Thread {
    static int a, b, c, m, n, x , y, z;
    int opcion;

    // CONSTRUCTOR
    public Ejercicio10(int opcion) {
        this.opcion = opcion;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("x: ");
        x = scanner.nextInt();
        System.out.print("b: ");
        b = scanner.nextInt();
        System.out.print("c: ");
        c = scanner.nextInt();

        System.out.println("--- INICIO DEL PROGRAMA ---");

//        Runnable runnableSec1 = new Ejercicio10(1);
//        Thread hiloSec1 = new Thread(runnableSec1);
        Thread hiloSec1 = new Ejercicio10(1);
        hiloSec1.start(); // S3

        a = x * x;
        System.out.println("S1: " + a);
        m = a * b;
        System.out.println("S2: " + m);

        try {
            hiloSec1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        Runnable runnableSec2 = new Ejercicio10(2);
//        Thread hiloSec2 = new Thread(runnableSec2);
        Thread hiloSec2 = new Ejercicio10(2);
        hiloSec2.start(); // S5

        y = m + n;
        System.out.println("S4: " + y);

        try {
            hiloSec2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("-- FIN DEL PRORAMA --");
    }

    @Override
    public void run() {
        switch (opcion) {
            case 1:
                // Hilo Secundario 1
                n = c * x;
                System.out.println("S3: " + n);
                break;
            case 2:
                // Hilo secundario 2
                z = m - n;
                System.out.println("S5: " + z);
                break;
        }
    }
}
