package com.company;

import java.util.Scanner;

public class HiloPrincipal {

    private int a, b, c, w; // a calcular por el programa

    // constructor (clase donde esta el main)
    public HiloPrincipal(int x, int y, int z) {
        System.out.println("--INICIO EJECUCIÓN--");
        HiloS2 miHiloS2 = new HiloS2(this, z);
        miHiloS2.start();
        a = x + y;
        System.out.printf("S1: a (%d) = x (%d) + y (%d);\n", a, x, y);
//        b = z - 1;
//        System.out.printf("S2: b (%d) = z (%d) - 1;\n", b, z);
        try {
            miHiloS2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        c = a - b;
        System.out.printf("S3: c (%d) = a (%d) - b (%d);\n", c, a, b);
        w = c + 1;
        System.out.printf("S4: w (%d) = c (%d) + 1;\n", w, c);
        System.out.println("--FIN EJECUCIÓN--");
    }

    public void setB(int b) {
        this.b = b;
    }

    // main (el inicio del programa)
    public static void main(String[] args) {
        int x, y, z; // a introducir por el usuario
        Scanner e = new Scanner(System.in);
        System.out.print("Introduce x: ");
        x = e.nextInt();
        System.out.print("Introduce y: ");
        y = e.nextInt();
        System.out.print("Introduce z: ");
        z = e.nextInt();
        HiloPrincipal miClase = new HiloPrincipal(x, y, z);
        e.close();
    }
}
