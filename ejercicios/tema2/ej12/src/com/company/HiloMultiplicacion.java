package com.company;

public class HiloMultiplicacion extends Thread {

    private int[] array;

    // constructor
    public HiloMultiplicacion(int[] array) {
        this.array = array;
    }

    @Override
    public void run() {
        long multiplicacion = 1;
        for (int numero : array) {
            multiplicacion *= numero;
        }
        System.out.println("MULTIPLICACION: " + multiplicacion);
    }
}
