package com.company;

public class HiloSuma extends Thread {

    private int[] array;

    // constructor
    public HiloSuma(int[] array) {
        this.array = array;
    }

    @Override
    public void run() {
        long suma = 0;
        for (int numero : array) {
            suma += numero;
        }
        System.out.println("SUMA: " + suma);
    }
}
