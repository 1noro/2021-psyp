package com.company;

import java.util.Random;
import java.util.Scanner;

public class HiloPrincipal {

    // constructor (clase donde esta el main)
    public HiloPrincipal() {
        System.out.println("--INICIO EJECUCIÓN--");
        Random rand = new Random();
        int min = 1;
        int max = 100;
        // calculamos el array
        int[] array = new int[100];
        for (int i = 0; i < array.length; i++) {
            int randomNum = rand.nextInt((max - min) + 1) + min;
            array[i] = randomNum;
        }
        Thread miHiloSuma = new HiloSuma(array);
        Thread miHiloMultiplicacion = new HiloMultiplicacion(array);
        miHiloSuma.start();
        miHiloMultiplicacion.start();
        try {
            miHiloSuma.join();
            miHiloMultiplicacion.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("--FIN EJECUCIÓN--");
    }

    // main (el inicio del programa)
    public static void main(String[] args) {
        HiloPrincipal miClase = new HiloPrincipal();
    }
}
