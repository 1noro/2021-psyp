package com.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    static Socket cs;
    static DataInputStream dis;
    static DataOutputStream dos;

    public static void main(String[] args) {
        try {
            Scanner s = new Scanner(System.in);
            cs = new Socket("127.0.0.1", 1234);
            dis = new DataInputStream(cs.getInputStream());
            dos = new DataOutputStream(cs.getOutputStream());

            double num;
            do {
                num = s.nextDouble();
                dos.writeDouble(num);
                if (num != 0) System.out.println(dis.readDouble());
            } while (num != 0);

            /*
            double num = s.nextDouble();
            while (num != 0) {
                dos.writeDouble(num);
                double result = dis.readDouble()
                System.out.println(result);
                num = s.nextDouble();
            }
            dos.writeDouble(num);
            */

            dis.close();
            dos.close();
            cs.close();
        } catch (Exception ignored) {}
    }

}
