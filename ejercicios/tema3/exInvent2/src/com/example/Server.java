package com.example;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    static ServerSocket ss;
    static Socket cs;
    static DataInputStream dis;
    static DataOutputStream dos;

    public static void main(String[] args) {
        try {
            ss = new ServerSocket(1234);
            while (true) {
                cs = ss.accept();
                dis = new DataInputStream(cs.getInputStream());
                dos = new DataOutputStream(cs.getOutputStream());
                double num = dis.readDouble();
                while (num != 0) {
                    dos.writeDouble(num * num);
                    num = dis.readDouble();
                }
                dis.close();
                dos.close();
                cs.close();
            }
        } catch (Exception ignored) {}
        finally {
            try {
                ss.close();
            } catch (Exception ignored) {}
        }
    }
}
