package com.afundaconfp;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
        try {
            // El nombre del archivo que le vamos a enviar al servidor para que lo lea y envíe su contenido de vuelta
            String filename = "ArchivoNombres.txt";

            // Creamos un socket vacío
            Socket clientSocket = new Socket();
            // Creamos una dirección que apunta a nuestro poropio equipo
            InetSocketAddress address = new InetSocketAddress("localhost", 1234);
            // Conectamos el socket vacío con el servidor a través de la dirección
            clientSocket.connect(address);

            // Obtenemos el outputStream del socket conectado con el servidor
            // (se usará para enviar información al servidor)
            OutputStream outputStream = clientSocket.getOutputStream();

            // Creamos el objeto printWriter sobre el outputStream para que nos facilite la escritura de
            // texto contra el servidor
            PrintWriter printWriter = new PrintWriter(outputStream, true);

            // Enviamos el nombre del archivo que queremos leer al servidor
            printWriter.println(filename);

            // Obtenemos el inputStream del socket conectado con el servidor
            // (se usará para ;eer lo que nos envíe el servidor)
            InputStream inputStream = clientSocket.getInputStream();

            // Creamos el objeto bufferedReader sobre el inputStream para que nos facilite la lectura del texto
            // que nos envía el servidor
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            // Definimos un array de chars de longitud 1 para que se vaya cargando de caracter en caracter según se
            // vayan recibiendo desde el servidor
            char[] buffer = new char[1];

            System.out.println("Descargnado fichero " + filename);

            // Leemos una primera vez y guardamos en el array de 1 el primer byte (1 byte = 8 bits = 1 char) de
            // información enviada desde el servidor y guardmos el número de caracteres leidos esta vez (siempre va
            // ser 1 excepto al acabar la lectura, que será -1)
            int numOfCharsReaded = bufferedReader.read(buffer);
            if (numOfCharsReaded != -1) { // si el primer caracter leído no es ya el final de la transmisón seguimos leyendo
                do {
                    System.out.print(buffer[0]); // imprmiomos lo que se ha leído en la función "read" y guardado en el array de 1 "buffer"
                } while (bufferedReader.read(buffer) != -1); // continuamos leyendo mientras no se acaben los bytes que leer
            }

            // cerramos todos los objetos abiertos antes en orden inverso al que los abrimos (no necesario)
            bufferedReader.close();
            inputStream.close();
            printWriter.close();
            outputStream.close();
            clientSocket.close();
        } catch (IOException e) {
            // Si cualquiera de las operaciones anteriores resultase erronea cascaría t0do el programa
            e.printStackTrace();
        }
    }
}
