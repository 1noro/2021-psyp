package com.afundaconfp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLOutput;

public class Server {

    public static void procesarCliente(Socket clientSocket) {
        DataInputStream dataInputStream = null;
        DataOutputStream dataOutputStream = null;
        try {
            System.out.println("New connection: " + clientSocket.getInetAddress());

            dataInputStream = new DataInputStream(clientSocket.getInputStream());
            dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

            BufferedReader bufferedReaderSocket = new BufferedReader(new InputStreamReader(dataInputStream));
            String filename = bufferedReaderSocket.readLine();

            BufferedReader bufferedReaderFile = new BufferedReader(new FileReader("/opt/jarchivos/" + filename));

            while (bufferedReaderFile.ready()) {
                char c = (char) bufferedReaderFile.read();
                dataOutputStream.write(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Error en la transmisión o recepción
        } finally {
            try {
                // cerramos las cosas abiertas
                assert dataInputStream != null;
                dataInputStream.close();
                assert dataOutputStream != null;
                dataOutputStream.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(1234);
        } catch (IOException e) {
            e.printStackTrace();
            // error al abrir el puerto
        }

        assert serverSocket != null;
        System.out.println("Servidor escuchando...");
        while (true) {
            try {
                Socket clientSocket = serverSocket.accept();
                procesarCliente(clientSocket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
