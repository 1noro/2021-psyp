package com.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(1234);
            // imprimir mierdas de ips (si lo pide)
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Nuevo cliente");
                DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
                int numRecibido;
                do {
                    int min = 1;
                    int max = 1001; // 1000
                    int random = (int) (min + (Math.random() * (max - min)));
                    System.out.println("Random: " + random);

                    do {
                        numRecibido = dataInputStream.readInt();
                        System.out.println("Recibido: " + numRecibido);
                        if (numRecibido < random && numRecibido != 0) {
                            dataOutputStream.writeUTF("UP");
                        } else if (numRecibido > random) {
                            dataOutputStream.writeUTF("DOWN");
                        }
                    } while (numRecibido != random && numRecibido != 0);

                    if (numRecibido == random) {
                        dataOutputStream.writeUTF("You are the winner baby!");
                    }
                } while (numRecibido != 0);

                System.out.println("Cliente finalizado!");

                dataOutputStream.close();
                dataInputStream.close();
                clientSocket.close(); // se cierra la conexión con el cliente
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
