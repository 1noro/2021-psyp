package com.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            Socket socket = new Socket("127.0.0.1", 1234);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            int numero;

            do {
                System.out.print("Escribe un número: ");
                numero = scanner.nextInt();
                dataOutputStream.writeInt(numero);
                if (numero != 0) {
                    String mensaje = dataInputStream.readUTF();
                    System.out.println(mensaje);
                }
            } while (numero != 0);
            System.out.println("Se acabó!");

            dataOutputStream.close();
            dataInputStream.close();
            socket.close(); // se cierra la conexión con el servidor
        } catch (IOException e) {
            e.printStackTrace();
        }
        scanner.close();
    }

}
