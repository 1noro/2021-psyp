package com.example;

import java.io.*;
import java.net.*;

/*
public class Server {

    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(1234);

            Socket cs = ss.accept();
            DataInputStream dis = new DataInputStream(cs.getInputStream());
            DataOutputStream dos = new DataOutputStream(cs.getOutputStream());
            double num = dis.readDouble();
            while (num != 0) {
                double result = 1;
                while (num != 0) {
                    result = result * num;
                    num--;
                }
                dos.writeDouble(result);
                num = dis.readDouble();
            }

            dis.close();
            dos.close();
            cs.close();
            ss.close();
        } catch (Exception ignored) {}
    }
}
*/

public class Server {

    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(1234);
            while (true) {
                Socket cs = ss.accept();
                DataInputStream dis = new DataInputStream(cs.getInputStream());
                DataOutputStream dos = new DataOutputStream(cs.getOutputStream());
                double num = dis.readDouble();

                while (num != 0) {
                    double result = 1;
                    while (num != 0) {
                        result = result * num;
                        num--;
                    }
                    dos.writeDouble(result);
                    num = dis.readDouble();
                }
                if (num == 0) {
                    dis.close();
                    dos.close();
                    cs.close();
                    break;
                }
                dis.close();
                dos.close();
                cs.close();
            }
            ss.close();
        } catch (Exception ignored) {}
    }
}


