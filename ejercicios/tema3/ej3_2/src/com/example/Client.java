package com.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            // Obtenemos la información de red de la máquina en la que se ejecuta el server
            InetAddress inetAddress = InetAddress.getLocalHost();
            System.out.println("IP: " + inetAddress.getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        System.out.print("Teclea un número: ");
        long numeroOut = scanner.nextLong();

        try {
            Socket socket = new Socket("127.0.0.1", 1234); // localhost en el puerto 1234

            // creamos la conexión para los datos de entrada
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            // creamos la conexión para los datos de salida
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

            // enviamos el número leido por teclado
            dataOutputStream.writeLong(numeroOut);

            // recibimos el número calculado por el server
            long numeroIn = dataInputStream.readLong();

            System.out.println("Resultado: " + numeroIn);

            // crerramos t0do lo abrible
            dataOutputStream.close();
            dataInputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
