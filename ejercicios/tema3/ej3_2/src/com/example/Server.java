package com.example;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class Server {
    public static void main(String[] args) {
        ServerSocket serverSocket = null; // asignamos null al serverSocket para luego ver si hay error
        try {
            serverSocket = new ServerSocket(1234);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert serverSocket != null; // si serverSocket es null aquí se acabó

        try {
            // Obtenemos la información de red de la máquina en la que se ejecuta el server
            InetAddress inetAddress = InetAddress.getLocalHost();
            System.out.println("Localhost: " + inetAddress);
            System.out.println("IP: " + inetAddress.getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        // por cada vuelta del bucle se recibe un cliente y se gestiona la petición
        while (true) {
            try {
                // obtenemos el socket por el que vamos a transmitir datos
                Socket clientSocket = serverSocket.accept();
                System.out.println("Nueva conexión (ip): " + clientSocket.getInetAddress());
                System.out.println("Nueva conexión (puerto): " + clientSocket.getPort()); // ?? getLocalPort

                // creamos la conexión para los datos de entrada
                DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
                // creamos la conexión para los datos de salida
                DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

                // obtenemos el número del cliente y calculamos el cuadrado
                long numeroIn = dataInputStream.readLong();
                long numeroOut = numeroIn * numeroIn;

                // enviamos el resultado al cliente
                dataOutputStream.writeLong(numeroOut);

                // cerramos t0do lo abierto
                dataOutputStream.close();
                dataInputStream.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
