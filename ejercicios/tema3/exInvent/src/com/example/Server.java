package com.example;

import java.io.*;
import java.net.*;

public class Server {
    static ServerSocket ss;
    static Socket cs;
    static DataInputStream dis;
    static DataOutputStream dos;
    public static void main(String[] args) {
        try {
            ss = new ServerSocket(1234);
            while (true) {
                cs = ss.accept();
                dis = new DataInputStream(cs.getInputStream());
                dos = new DataOutputStream(cs.getOutputStream());
                double in = dis.readDouble();
                dos.writeDouble(in * in);
                dis.close();
                dos.close();
                cs.close();
            }
        } catch (Exception ignored) {} finally {
            try {
                ss.close();
            } catch (Exception ignored) {}
        }
    }
}
