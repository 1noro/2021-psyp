package com.afundaconfp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final String serverIp = "localhost";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Number to send: ");
        int number = scanner.nextInt();

        try {
            Socket callSocket = new Socket(serverIp, 1234);

            DataInputStream dataInputStream = new DataInputStream(callSocket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(callSocket.getOutputStream());

            dataOutputStream.writeInt(number);

            long result = dataInputStream.readLong();

            System.out.println("Result: " + result);

            // cerramos las coasas abiertas
            dataInputStream.close();
            dataOutputStream.close();
            callSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
            // Algo falló en la recepción o transmisión
        }

    }
}
