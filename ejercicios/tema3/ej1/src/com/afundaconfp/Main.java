package com.afundaconfp;

import java.net.UnknownHostException;

import static java.net.InetAddress.getByName;
import static java.net.InetAddress.getLocalHost;

public class Main {

    public static void main(String[] args) {
        try {
            System.out.println("La dirección IP de una URL por nombre (www.google.com):");
            System.out.println(getByName("www.google.com").getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            System.out.println("Nombre a partir de la dirección (216.58.211.36):");
            System.out.println(getByName("216.58.211.36").getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            System.out.println("Dirección IP actual de LocalHost:");
            System.out.println(getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            System.out.println("Nombre de LocalHost a partir de la dirección:");
            System.out.println(getByName("127.0.0.1").getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            System.out.println("Nombre actual del LocalHost:");
            System.out.println(getLocalHost().getCanonicalHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
