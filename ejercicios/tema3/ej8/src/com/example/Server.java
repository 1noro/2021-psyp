package com.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(1234);
            while (true) {
                Socket clientSocket = serverSocket.accept(); // cuando se conecta un cliente obtenemos su socket
                System.out.println("Nuevo cliente: " + clientSocket.getInetAddress());
                DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

                String mensaje;
                do {
                    mensaje = dataInputStream.readUTF();
                    dataOutputStream.writeUTF(mensaje);
                } while (!mensaje.equalsIgnoreCase("adios"));

                System.out.println("Cliente cerrado");

                dataInputStream.close();
                dataOutputStream.close();
                clientSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
