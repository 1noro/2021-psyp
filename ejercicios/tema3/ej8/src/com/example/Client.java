package com.example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            Socket socket = new Socket("127.0.0.1", 1234);
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());

            String mensaje;
            do {
                System.out.print("Escribe algo: ");
                mensaje = scanner.nextLine();
                dataOutputStream.writeUTF(mensaje);
                if (!mensaje.equalsIgnoreCase("adios")) {
                    System.out.println(dataInputStream.readUTF());
                }
            } while (!mensaje.equalsIgnoreCase("adios"));

            System.out.println("Se acabó!");

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        scanner.close();
    }
}
