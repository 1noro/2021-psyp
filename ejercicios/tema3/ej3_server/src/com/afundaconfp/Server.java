package com.afundaconfp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(1234);
        } catch (IOException e) {
            e.printStackTrace();
            // error al abrir el puerto
        }

        while (true) {
            try {
                assert serverSocket != null;
                Socket clientSocket = serverSocket.accept();

                System.out.println("New connection: " + clientSocket.getInetAddress());

                DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
                DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

                long numberIn = dataInputStream.readInt();
                long numberOut = numberIn * numberIn;

                dataOutputStream.writeLong(numberOut);

                // cerramos las cosas abiertas
                dataInputStream.close();
                dataOutputStream.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
                // Error en la transmisión o recepción
            }
        }
    }
}
