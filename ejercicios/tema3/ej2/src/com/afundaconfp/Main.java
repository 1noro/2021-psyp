package com.afundaconfp;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {



    public static void main(String[] args) {

        try {
            InetAddress dir = null;

            System.out.println("getByName:");
            dir = InetAddress.getByName("koi");
            System.out.println(dir);

            System.out.println("getLocalHost:");
            dir = InetAddress.getLocalHost();
            System.out.println(dir);

            System.out.println("getHostName:");
            System.out.println(dir.getHostName());

            System.out.println("getHostAddress:");
            System.out.println(dir.getHostAddress());

            System.out.println("toString:");
            System.out.println(dir.toString());

            System.out.println("getCanonicalHostName:");
            System.out.println(dir.getCanonicalHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        System.out.println("#########################################");

        try {
            InetAddress dir = null;

            System.out.println("getByName:");
            dir = InetAddress.getByName("www.google.com");
            System.out.println(dir);

            System.out.println("getHostName:");
            System.out.println(dir.getHostName());

            System.out.println("getHostAddress:");
            System.out.println(dir.getHostAddress());

            System.out.println("toString:");
            System.out.println(dir.toString());

            System.out.println("getCanonicalHostName:");
            System.out.println(dir.getCanonicalHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
